# Git: Common Commands Suite

## <strong style="color:green">Now Supports Access From Explorer/Editor Context Menus!</strong>

## Installation
Run `ext install git-common-commands-suite` from your VS Code CTRL/CMD+P window.

## Setup
### For the extension to work, your folder system needs to be set-up in a specific way.
1. The root folder needs to have your .git folder in it.
2. Your credentials must be stored for the current project.

#### To do step 2, choose either of the following:
* [Temporary Credential Store](https://git-scm.com/docs/git-credential-cache)
* [Permanent Credential Store](https://git-scm.com/docs/git-credential-store)

<strong style="color:red;font-style:italic">WARNING:</strong><span style="font-style:italic"> The second method will store your credentials in plaintext, not encrypted, which is insecure</span>

## Suggestions
If you have any suggestions for other useful commands you'd like to see in this suite, please request the feature on the GitLab page.

----

## Git: Add Remote

### Usage
To use the command, press F1 in the Code window, and type:

    Git: Add Remote

When the pop-up appears, enter your Remote's name and press < Enter >.

Another pop-up will appear, where you enter the Git Repository's URL you wish to add, and press < Enter >.



## Git: List/Remove Remotes

### Usage
To use the command, press F1 in the Code window, and type:

    Git: List Remotes

A list of remotes will appear under the Command Palette.

If you click on a remote, its verbose information will appear.

You can also remove a remote from the project. This can be done through the verbose information drop-down.

A Remove option will be available, and when clicked, will remove the remote.



## Git: Pull - (Remote/Branch)  <strong style="color:orange;font-style:italic">(Updated)</strong>

### Usage
To use the command, press F1 in the Code window, and type:

    Git: Pull (Remote/Branch Specified)

When the pop-up appears, enter your Remote's name and press < Enter >.

Another pop-up will appear, where you enter the Branch you wish to pull from, and press < Enter >.

A file is saved to your user directory which will store you previous entries for quick access.

<strong style="color:slategray">Note:</strong> To make good use of this command, you should setup other remotes alongside origin. VS Code's built in command Git: Pull will pull from the repository you have set as the upstream, which defaults to your origin remote, so if your only remote is origin, you are better off using Git: Pull to do pulls as it will be more efficient than this command.



## Git: Push - (Remote/Branch)

### Usage
To use the command, press F1 in the Code window, and type:

    Git: Push (Remote/Branch Specified)

When the pop-up appears, enter your Remote's name and press < Enter >.

Another pop-up will appear, where you enter the Branch you wish to push to, and press < Enter >.

<strong style="color:slategray">Note:</strong> To make good use of this command, you should setup other remotes alongside origin. VS Code's built in command Git: Push will push to the repository you have set as the upstream, which defaults to your origin remote, so if your only remote is origin, you are better off using Git: Push to do pushes as it will be more efficient than this command.



## Git: Status

### Usage
To use the command, press F1 in the Code window, and type:

    Git: Status

Status information will appear under the Command Palette.

If you click on one of the file list entries in the pop-up, it will display the file list for that type. (e.g.: You select 'Number of Modified Files: ', a list of your modified files will appear)

----

## Individually Packaged Extensions
* [Git: Add Remote](https://marketplace.visualstudio.com/items?itemName=samschneller.git-add-remote)
* [Git: Pull - Remote/Branch Specified](https://marketplace.visualstudio.com/items?itemName=samschneller.git-pull-upstream)
* Git: Status (WIP)
* Git: Push - Remote/Branch Specified (WIP)

----

## Dependencies
* Simple Git - Used to simplify interfacing with Git
  * [GitHub Page](https://github.com/steveukx/git-js)
  * [npm Page](https://www.npmjs.com/package/simple-git)

* JavaScript Promises

----

## License
[MIT](http://spdx.org/licenses/MIT)

----

## Source
[GitLab](https://gitlab.com/MadCrayolaz/git-common-commands-suite)

----

## Changelog

* 5/11/16 - v0.1.4: Updated README With New Link To This Repository
* 7/27/16 - v0.2.2: Added functionality to Pull where previous entries are saved to disk