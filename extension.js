var vscode = require('vscode'),
    simpleGit = require('simple-git')((vscode.workspace.rootPath) ? vscode.workspace.rootPath : '.'),
    fs = require('fs');

function activate(context) {
    var disposable = vscode.commands.registerCommand('extension.addRemote', function(cmds) {
        var remoteName,
            remoteURL,
            promise = new Promise(function(resolve, reject) {
                // do a thing, possibly async, then…
                remoteName = vscode.window.showInputBox({prompt: 'Enter Remote Name'});
                if(remoteName) {
                    resolve(remoteName);
                }
                else {
                    reject(Error('It broke'));
                }
            });

        promise.then(function(result) {
            var promise2 = new Promise(function(resolve2, reject2) {
                // do a thing, possibly async, then…
                remoteURL = vscode.window.showInputBox({prompt: 'Enter Remote Git Url'});
                if(remoteURL) {
                    resolve2(remoteURL);
                }
                else {
                    reject2(Error('It broke'));
                }
            });

            promise2.then(function(result2) {
                vscode.window.showInformationMessage('Adding remote ' + remoteName._value + ' located @ ' + remoteURL._value);
                simpleGit.addRemote(remoteName._value, remoteURL._value);
            }, function(err2) {
                console.log(err); // Error: 'It broke'
            });
        }, function(err) {
            console.log(err); // Error: 'It broke'
        });
    });

    var disposable2 = vscode.commands.registerCommand('extension.status', function(cmds) {
        var checking = simpleGit.status(function(err, data) {
            var stArray = [],
                stArray2 = [];
            console.log(data);
            if(data) {
                if(data.current) {
                    stArray.push('On branch \"' + data.current + '\"');
                }
                if(data.ahead > 0 && data.tracking) {
                    stArray.push('Your branch is ahead \'' + data.tracking + '\' by ' + data.ahead + ' commits.');
                }
                else if(data.behind > 0 && data.tracking) {
                    stArray.push('Your branch is behind \'' + data.tracking + '\' by ' + data.behind + ' commits.');
                }
                else {
                    if(data.tracking) {
                        stArray.push('Your branch is up-to-date with \'' + data.tracking + '\'.');
                    }
                }
                if(data.modified && data.modified.length > 0) {
                    stArray.push('Number of Modified Files: ' + data.modified.length);
                }
                if(data.created && data.created.length > 0) {
                    stArray.push('Number of Created Files: ' + data.created.length);
                }
                if(data.deleted && data.deleted.length > 0) {
                    stArray.push('Number of Deleted Files: ' + data.deleted.length);
                }
                if(data.not_added && data.not_added.length > 0) {
                    stArray.push('Number of Untracked Files: ' + data.not_added.length);
                }
                vscode.window.showQuickPick(stArray, {placeHolder: 'Click an option to see the specific files'})
                    .then(val => {
                        if(val.indexOf('Modified') > 0) {
                            data.modified.forEach(function(element, index) {
                                stArray2.push('Modified: ' + element);
                                if(index === data.modified.length - 1) {
                                    vscode.window.showQuickPick(stArray2);
                                }
                            });
                        }
                        else if(val.indexOf('Created') > 0) {
                            data.created.forEach(function(element, index) {
                                stArray2.push('Created: ' + element);
                                if(index === data.created.length - 1) {
                                    vscode.window.showQuickPick(stArray2);
                                }
                            });
                        }
                        else if(val.indexOf('Deleted') > 0) {
                            data.deleted.forEach(function(element, index) {
                                stArray2.push('Deleted: ' + element);
                                if(index === data.deleted.length - 1) {
                                    vscode.window.showQuickPick(stArray2);
                                }
                            });
                        }
                        else if(val.indexOf('Untracked') > 0) {
                            data.not_added.forEach(function(element, index) {
                                stArray2.push('Untracked: ' + element);
                                if(index === data.not_added.length - 1) {
                                    vscode.window.showQuickPick(stArray2);
                                }
                            });
                        }
                    });
            }
            else {
                vscode.window.showErrorMessage('No Status Found For Current Project');
            }
        });
    });

    var disposable3 = vscode.commands.registerCommand('extension.doPull', function(cmds) {
        if(fs.readdirSync('.').indexOf('previousEntries.json') > -1) {
            var combos = ['New Entry'];
            fs.readFile('./previousEntries.json', function(err, data) {
                var comboObjects = JSON.parse(data);
                comboObjects.forEach(function(e, i) {
                    combos.push(e.remote + '/' + e.branch);
                    if(i === comboObjects.length - 1) {
                        vscode.window.showQuickPick(combos, {placeHolder: 'Search Previous Remotes...'})
                            .then(val => {
                                if(val === 'New Entry') {
                                    pullFunc(function(err) {
                                        if(err) {
                                            console.log('ERR:', err);
                                        }
                                        else {
                                            console.log('NO ERR');
                                        }
                                    });
                                }
                                else {
                                    vscode.window.showInformationMessage('Pulling from ' + val);
                                    simpleGit.pull(val.split('/')[0], val.split('/')[1]);
                                }
                            });
                    }
                });
            });
        }
        else {
            pullFunc(function(err) {
                if(err) {
                    console.log('ERR:', err);
                }
                else {
                    console.log('NO ERR');
                }
            });
        }
    });

    var disposable4 = vscode.commands.registerCommand('extension.doPush', function(cmds) {
        var remote;
            branch,
            promise = new Promise(function(resolve, reject) {
                // do a thing, possibly async, then…
                remote = vscode.window.showInputBox({prompt: 'Enter Remote Name'});
                if(remote) {
                    resolve(remote);
                }
                else {
                    reject(Error('It broke'));
                }
            });

        promise.then(function(result) {
            var promise2 = new Promise(function(resolve2, reject2) {
            // do a thing, possibly async, then…
                branch = vscode.window.showInputBox({prompt: 'Enter Branch Name'});
                if(branch) {
                    resolve2(branch);
                }
                else {
                    reject2(Error('It broke'));
                }
            });

            promise2.then(function(result2) {
                vscode.window.showInformationMessage('Pushing to ' + result + '/' + branch._value);
                simpleGit.pull(result, branch._value);
            }, function(err2) {
                console.log(err); // Error: 'It broke'
            });
        }, function(err) {
            console.log(err); // Error: 'It broke'
        });
    });

    var disposable5 = vscode.commands.registerCommand('extension.listRemotes', function(cmds) {
        simpleGit.getRemotes(true, function(err, result) {
            var stArray = [],
                stArray2 = [],
                chosenName;
            if(result) {
                result.forEach(function(element, index) {
                    stArray.push(element.name);
                    if(index === result.length - 1) {
                        vscode.window.showQuickPick(stArray, {placeHolder: 'Click on a remote to see its verbose information'})
                            .then(val => {
                                result.forEach(function(element2, index2) {
                                    if(element2.name === val) {
                                        stArray2.push('fetch: ' + element2.refs.fetch);
                                        stArray2.push('push: ' + element2.refs.push);
                                        stArray2.push('[x] Remove');
                                        chosenName = element2.name;
                                    }
                                    if(index2 === result.length - 1) {
                                        vscode.window.showQuickPick(stArray2, {placeHolder: 'Click Remove to remove the remote'})
                                            .then(val2 => {
                                                result.forEach(function(element3, index3) {
                                                    if(val2 === '[x] Remove') {
                                                        vscode.window.showInformationMessage('Removing remote ' + chosenName + '.');
                                                        simpleGit.removeRemote(chosenName);
                                                    }
                                                });
                                            });
                                    }
                                });
                            });
                    }
                });
            }
        });
    });

    var pullFunc = function(next) {
        var remote,
            branch,
            promise = new Promise(function(resolve, reject) {
                // do a thing, possibly async, then…
                remote = vscode.window.showInputBox({prompt: 'Enter Remote Name'});
                if(remote) {
                    resolve(remote);
                }
                else {
                    reject(Error('It broke'));
                }
            });

        promise.then(function(result) {
            var promise2 = new Promise(function(resolve2, reject2) {
                // do a thing, possibly async, then…
                branch = vscode.window.showInputBox({prompt: 'Enter Branch Name'});
                if(branch) {
                    resolve2(branch);
                }
                else {
                    reject2(Error('It broke'));
                }
            });

            promise2.then(function(result2) {
                vscode.window.showInformationMessage('Pulling from ' + result + '/' + branch._value);
                simpleGit.pull(result, branch._value);
                if(fs.readdirSync('.').indexOf('previousEntries.json') > -1) {
                    fs.readFile('./previousEntries.json', function(err, data) {
                        var combos = JSON.parse(data);
                        combos.push({remote: result, branch: branch._value});
                        fs.writeFile('previousEntries.json', JSON.stringify(combos), function(err) {
                            next(err);
                        });
                    });
                }
                else {
                    fs.writeFile('previousEntries.json', JSON.stringify([{remote: result, branch: branch._value}]), function(err) {
                        next(err);
                    });
                }
            }, function(err2) {
                console.log(err); // Error: 'It broke'
            });
        }, function(err) {
            console.log(err); // Error: 'It broke'
        });
    }

    context.subscriptions.push(disposable);
    context.subscriptions.push(disposable2);
    context.subscriptions.push(disposable3);
    context.subscriptions.push(disposable4);
    context.subscriptions.push(disposable5);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {
}
exports.deactivate = deactivate;
